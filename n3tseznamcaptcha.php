<?php
/**
 * @package n3t Seznam Captcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2012-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Http\HttpFactory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\Utilities\IpHelper;

jimport( 'joomla.plugin.plugin' );

class plgCaptchaN3tSeznamCaptcha extends CMSPlugin
{

  public function __construct($subject, $config)
  {
    parent::__construct($subject, $config);
    $this->loadLanguage();
  }
  
  protected function getIp()
  {
    jimport( 'fof.utils.ip.ip' );

    if (class_exists('FOFUtilsIp'))
      return FOFUtilsIp::getIp();

    return IpHelper::getIp();
  }

  protected function isIPv6($ip)
  {
    jimport( 'fof.utils.ip.ip' );

    if (class_exists('FOFUtilsIp'))
      return FOFUtilsIp::isIPv6($ip);

    return IpHelper::isIPv6($ip);
  }

  protected function IPinList($ip, $ipTable = '')
  {
    jimport( 'fof.utils.ip.ip' );

    if (class_exists('FOFUtilsIp'))
      return FOFUtilsIp::IPinList($ip, $ipTable);

    return IpHelper::IPinList($ip, $ipTable);
  }

  public function onInit($id)
  {
    if (!$this->params->def('enable_captcha', 1)) return;

    $doc = Factory::getDocument();
    if ($this->params->def('theme', 'dark-icons') != 'none')
      HTMLHelper::_('stylesheet', 'plg_n3tseznamcaptcha/' . $this->params->def('theme', 'dark-icons').'.min.css', ['version' => 'auto', 'relative' => true]);
   
    HTMLHelper::_('jquery.framework');
    HTMLHelper::_('jquery.ui');
    HTMLHelper::_('script', 'plg_n3tseznamcaptcha/jquery.captcha.min.js', ['version' => 'auto', 'relative' => true]);
    $doc->addScriptDeclaration("
      jQuery(function(){
        jQuery('.seznam-captcha').n3tSeznamCaptcha({
          'url': '".addslashes(URI::root())."'
        });
      });"
    );
  }

  public function onDisplay($name, $id, $class)
  {
    if (!$this->params->def('enable_captcha', 1))
      return;

    return '<div class="seznam-captcha loading"></div>';
  }

  public function onCheckAnswer($code)
  {
    $input = Factory::getApplication()->input;

    if ($this->params->def('enable_captcha', 1)) {
      $hash = $input->get('n3t_seznam_captcha_hash', '', 'cmd');       
      $answer = $input->get('n3t_seznam_captcha', '', 'cmd');

      if ($hash == null || strlen($hash) == 0 || $answer == null || strlen($answer) == 0)
        throw new \RuntimeException(Text::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_EMPTY_ANSWER'), 500);

      if (!$this->_checkAnswer($hash, $answer))
        throw new \RuntimeException(Text::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_WRONG_ANSWER'), 500);
    }

    $check = array();
    $check['ip'] = $this->getIp();
    $check['ipv6'] = $this->isIPv6($check['ip']);
    if ($check['ipv6'])
      $check['reverseip'] = implode(':',array_reverse(explode(':',$check['ip'])));    
    else
      $check['reverseip'] = implode('.',array_reverse(explode('.',$check['ip'])));

    $ip_whitelist = preg_split('/\s*\n\s*/', $this->params->def('ip_whitelist', ''));
    if ($this->IPinList($check['ip'], $ip_whitelist))
      return true;

    $ip_blacklist = preg_split('/\s*\n\s*/', $this->params->def('ip_blacklist', ''));
    if ($this->IPinList($check['ip'], $ip_blacklist))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    if ($this->params->def('check_stopforumspam', '0') && !$this->_checkStopForumSpam($check))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    if ($this->params->def('check_botscout', '0') && !$this->_checkBotScout($check))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    if ($this->params->def('check_spamhaus', '0') && !$this->_checkSpamHaus($check))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    if ($this->params->def('check_sorbs', '0') && !$this->_checkSorbs($check))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    if ($this->params->def('check_spamcop', '0') && !$this->_checkSpamCop($check))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    if ($this->params->def('check_honeypot', '0') && !$this->_checkHoneyPot($check))
      throw new \RuntimeException(Text::sprintf('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_BLACKLIST', $check['ip']), 500);

    return true;
  }

  function onAjaxN3tseznamcaptcha() {
    $app = Factory::getApplication();
    $input = $app->input;
    switch($input->get('task', '', 'cmd')) {
      case 'layout':
      default:
        $path = PluginHelper::getLayoutPath($this->_type, $this->_name, $this->params->def('theme', 'dark-icons'));
        ob_start();
        include $path;
        return ob_get_clean();
        break;
        
      case 'create':
        $app->setHeader('Cache-Control', 'max-age=0,no-cache,no-store,post-check=0,pre-check=0,must-revalidate', true);
        $app->setHeader('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT', true);

        return $this->_getUrl('http://captcha.seznam.cz/captcha.create');
        break;        

      case 'audio':
        $audio = $this->_getUrl('http://captcha.seznam.cz/captcha.getAudio?hash='.$input->get('hash'));

        $app->setHeader('Cache-Control', 'max-age=0,no-cache,no-store,post-check=0,pre-check=0,must-revalidate', true);
        $app->setHeader('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT', true);
        $app->setHeader('Content-Type', 'audio/x-wav', true);
        $app->setHeader('Content-Disposition', 'attachment; filename=captcha.wav', true);        
        $app->setHeader('Content-Length', strlen($audio), true);
        
        return $audio;
        break;        
    }
  }
  
  private function _checkAnswer($hash, $answer)
  {
    $url = 'http://captcha.seznam.cz/captcha.check?hash=' . $hash . '&code=' . $answer;
    try {
      $http = HttpFactory::getHttp();
      $response = $http->get($url);
      return $response->code == 200;
    } catch (Exception $e) {
      $this->setError($e->getMessage());
      return false;
    }
  }

  private function _getUrl($url)
  {
    try {
      $http = HttpFactory::getHttp();
      $response = $http->get($url);
      return $response->body;
    } catch (Exception $e) {
      return '';
    }
  }

  private function _checkStopForumSpam($data)
  {
    if ($data['ip'] && !$data['ipv6']) {
      $address = 'http://api.stopforumspam.org/api?f=serial&ip='.$data['ip'];
      $response = $this->_getUrl($address);
      if ($response) {
        $response = unserialize($response);
        if ($response['success'] && $response['ip']['appears']) return false;
      }
    }
    return true;
  }

  private function _checkSorbs($data)
  {
    if ($data['reverseip']) {
      $address = $data['reverseip'].'.l2.spews.dnsbl.sorbs.net.';
      $response = @gethostbyname($address);
      if ($response != $address) return false;

      $address = $data['reverseip'].'.problems.dnsbl.sorbs.net.';
      $response = @gethostbyname($address);
      if ($response != $address) return false;
    }
    return true;
  }

  private function _checkBotScout($data)
  {
    if ($data['ip']) {
      $address = 'http://botscout.com/test/?ip=' . $data['ip'];
      if ($this->params->def('botscout_api_key', ''))
        $address .= '&key=' . $this->params->get('botscout_api_key');
      $response = $this->_getUrl($address);
      if ($response) {
        $response = explode('|', $response);
        if ($response[0] == 'Y') return false;
      }
    }
    return true;
  }

  private function _checkSpamHaus($data)
  {
    if ($data['reverseip']) {
      $address = $data['reverseip'] . '.zen.spamhaus.org.';
      $response = @gethostbyname($address);
      if ($response != $address) return false;
    }
    return true;
  }

  private function _checkSpamCop($data)
  {
    if ($data['reverseip']) {
      $address = $data['reverseip'] . '.bl.spamcop.net.';
      $response = @gethostbyname($address);
      if ($response == '127.0.0.2') return false;
    }
    return true;
  }

  private function _checkHoneyPot($data)
  {
    if (!$data['ipv6'] && $data['reverseip'] && $this->params->def('honeypot_api_key', '')) {
      $address = $this->params->get('honeypot_api_key') . '.' . $data['reverseip'].'.dnsbl.httpbl.org.';
      $response = @gethostbyname($address);
      if ($response != $address) {
        $response = explode('.',$response);
        if ($response[2] > 0 && $response[3] > 1) return false;
      }
    }
    return true;
  }
}