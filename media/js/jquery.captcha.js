/**
 * @package n3t Seznam Captcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2012-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

/* globals jQuery */

(function($) {
  "use strict";
  $.fn.n3tSeznamCaptcha = function(options) {

    var browser = {};
    browser.audio = window.Audio !== null;
    if (browser.audio) {
      var audio = document.createElement('audio');
      if (!!audio.canPlayType) {
        browser.audio = audio.canPlayType('audio/wav; codecs="1"').replace(/^no$/,'') !== '';
      } else {
        browser.audio = false;
      }
    }
    browser.embed = navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0;
    browser.embed = browser.embed || (navigator.mimeTypes["audio/wav"] && navigator.mimeTypes["audio/wav"].enabledPlugin);

    var settings = $.extend({}, $.fn.n3tSeznamCaptcha.options, options );

    return this.each(function() {
      var form = $(this).parents('form');
      var captcha = {};
      captcha.settings = settings;
      captcha.wrapper = $(this);
      captcha.player = false;
      captcha.audio = function() {
        var url = captcha.wrapper.find('.seznam-captcha-audio').attr('href');
        if (browser.audio) {
          if (!captcha.player) {
            captcha.player = new Audio(url);
          }
          captcha.player.play();
        } else if (browser.embed) {
          if (!captcha.player) {
            captcha.player = $('<embed src="'+url+'" autostart="true" hidden="true" width="0" height="0" enablejavascript="true" type="audio/x-wav"></embed>');
            captcha.wrapper.append(captcha.player);
          } else {
            captcha.player.get(0).play();
          }
        }
      };
      captcha.reload = function() {
        captcha.wrapper.addClass('loading');
        if (captcha.player) {
          if (browser.audio) {
            captcha.player.pause();
          } else if (browser.embed) {
            captcha.player.remove();
          }
          captcha.player = false;
        }
        captcha.clear();
        $.get(captcha.settings.url+'index.php?option=com_ajax&plugin=n3tseznamcaptcha&group=captcha&task=create&format=raw',
          function(hash) {
            captcha.wrapper.find('input[name=n3t_seznam_captcha_hash]').val(hash);
            captcha.wrapper.find('.seznam-captcha-image').attr('src','https://captcha.seznam.cz/captcha.getImage?hash='+hash);
            if (browser.audio || browser.embed) {
              captcha.wrapper.find('.seznam-captcha-audio').attr('href','https://captcha.seznam.cz/captcha.getAudio?hash='+hash);
            } else {
              captcha.wrapper.find('.seznam-captcha-audio').attr('href',captcha.settings.url+'index.php?option=com_ajax&plugin=n3tseznamcaptcha&group=captcha&task=audio&hash='+hash+'&format=raw');
            }
            captcha.wrapper.removeClass('loading');
          }
        );
      };
      captcha.clear = function() {
        captcha.wrapper.find('input[name=n3t_seznam_captcha]').val('');
      };

      form.data('n3tseznamcaptcha',captcha);

      captcha.wrapper.load(captcha.settings.url+'index.php?option=com_ajax&plugin=n3tseznamcaptcha&group=captcha&format=raw',
        function() {
          captcha.wrapper.find('.seznam-captcha-image');
          if (browser.audio || browser.embed) {
            captcha.wrapper.find('.seznam-captcha-audio').click(function(e) {
              captcha.audio();
              e.preventDefault();
            });
          }
          captcha.wrapper.find('.seznam-captcha-reload').click(function(e) {
            captcha.reload();
            e.preventDefault();
          });
          captcha.reload();
        }
      );
    });
  };

  $.fn.n3tSeznamCaptcha.options = {
    'url': ''
  };
}(jQuery));