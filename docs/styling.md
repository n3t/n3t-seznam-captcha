Custom styling
==============

Any part of n3t Seznam Captcha output could be overwritten in your template or Joomla!
instance. Depending of what kind of behavior you want to change, read proper chapter.

Langauge overrides
------------------

The most simple thing, you could override are some texts. There are very few texts 
in n3t Seznam Captcha, so this would be simple task.

If your Joomla! instance is running different language than english, and you still
see english messages on your Captcha field, check first, if there is available 
[translation to your language](about/i18n.md). If not, please consider to contribute
and add this translation. It is quite simple task, using Transifex online translation
environment.

If you found some text not to be suitable for your site, and you would like to change 
it little bit, just go to your administration, menu Extension - Langauge manager
and choose overrides on the left side menu.

In the filter field, which will appear on the left side, just choose your language
and Administrator (seems maybe strange, as you want to translate site, but plugins 
has stored their language files in administrator area). So for english you should
choose filter named something like `English (en-GB) - Administrator`.

Now click on `New` icon in toolbar. This will open a form to add new language override.
If you do not know the name of language constant, you want to change (and if you are 
reading this guide, you propably don't), use the search form on the right side, 
and enter at least part of the text, you want to change. List of available text
constants will be displayed. Just click on the one, you want to change. It will 
be copied to the form on the left, where, in the `Text` field you can change, what 
it says.

To find all texts used in n3t Seznam Captcha change in search form to search on 
`Constant` instead of `Value` and enter `N3TSEZNAMCAPTCHA` in the search field. 

Themes CSS override
-------------------

If you like on of pre-defined themes, but you want to change it little bit, you 
have generally two options, either disable the theme and include its CSS and images
in you template directly, or use so called template override.

Template override is quite simple, just go to `/media/plg_n3tseznamcaptcha/css`
folder on your FTP, and copy from there any CSS you want to change.

Make your changes and upload your changed CSS to 
`/templates/YOUR_TEMPLATE/css/plg_n3tseznamcaptcha` folder on your FTP. That is all.

Note, that if you copy for example Light or Dark Theme CSS file, you should also 
change path to icons used in these files, or copy those icons into your template also.
As CSS is targeting image files using relative paths, you borwser will look for these
icons relatively to CSS file location. 

HTML override
-------------

If, for some reason, you wnat to change HTML produced by n3t Seznam Captcha, it 
could be done again with template override.

In this case, original files could be found in `/plugins/captcha/n3tseznamcaptcha/tmpl`.
Currently there are 3 files in this location (in future releases could be more), 
each theme is using one of these files, so you propably would need to copy just one. 

Select file which is named according theme you use (for Bootstrap it would be 
`bootstrap.php` for example). If there is no file named as your theme, copy `default.php`,
which is fallback file (used by default, if there is no theme specific HTML file).

Now copy this file to `/templates/YOUR_TEMPLATE/html/plg_captcha_n3tseznamcaptcha`
folder, and make your changes.

Note, that some of CSS classes are necessary for proper plugin functionality (at 
least, if you use original JavaScript file). These are those starting with `seznam-captcha`.
Also naming of inputs should stayed untouched. Generally, change these files, only
if you __really know, what you do__, as you can easilly make your Captcha not working. 

JavaScript override
-------------------

Finally, if JavaScript used by n3t Seznam Captcha needs to be overriden, again,
the solution is template override.

In this case, you need to copy files from `/media/plg_n3tseznamcaptcha/js` to
`/templates/YOUR_TEMPLATE/js/plg_n3tseznamcaptcha`, make your changes, and you 
are done.

Why to use template override?
-----------------------------

Maybe you are confused, why to copy some files, if you can change its contents 
directly in original location. The answer is very simple, you __keep backward 
compatibility__.

If you change contents of these files in its originall location, you will __loose
your changes__ upon next upgrade. So if you want to upgrade (and this is always
recommended through possible occurance of security issues), and do not want to 
make your changes after every upgrade, use template override. 
