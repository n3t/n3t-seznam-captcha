License
=======

n3t Seznam Captcha is released under [GNU/GPL v3][GNUGPL] license.

For Seznam.cz Captcha API license agreement [see here][CaptchaLicense] (Czech 
language only).

[GNUGPL]: http://www.gnu.org/licenses/gpl-3.0.html
[CaptchaLicense]: http://captcha-api.seznam.cz/agreement/