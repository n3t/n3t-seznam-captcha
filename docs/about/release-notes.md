Release notes
=============

4.0.x
-----

#### 4.0.1
- IE11 javascript bug solved

#### 4.0.0

- Joomla! 4 compatibility
- Improved installer
- Bootstrap 4 theme
- Reload Captcha could fail when used after playing audio
- Joomla 4 css files naming convention (used .min.css) - backward incompatible in case of template override

3.0.x
-----

#### 3.0.2

- use https seznam server address instead of http

#### 3.0.1

- Better guest IP address detection
- IP blacklist and whitelist could contain ranges now
- IPv6 support

#### 3.0.0

- Joomla 2.5 support discontinued, Joomla 3.4 and later is supported now
- new major version to follow Joomla versioning
- Plugin renamed to follow JED standards
- IE 11 audio captcha support
- Placeholder text and title on CAPTCHA input
- hasTooltip class on buttons to enable nice Tooltips
- New Bootstrap theme
- New Bootstrap 3 theme for Bootstrap 3 based templates
- SpamBusted.com Support removed
- Loading indicator option - loading indicator shows always now
- Mootols code changed to jQuery
- HTML now created by layout - supports template override
- Minor code improvements
- New Help site
- New Update site

1.2.x
-----

#### 1.2.0

- Option to disable visual CAPTCHA
- vi-VN translation - thanks to Thu Hoai at Transifex

1.1.x
-----

#### 1.1.0

- Loading indicator option
- optional check of user's IP address against multiple bots and spam databases
- IP blacklist and whitelist
- pl-PL translation - thanks to Krzysztof Wandas at Transifex

1.0.x
-----

#### 1.0.4

- Rely on get_headers function rather then on $http_response_header in PHP 5.3
- de-DE translation - thanks to Stefan Kreitz at Transifex

#### 1.0.3

- Input field is now marked with autocomplete off
- Input field is now marked as required, so forms should validate it is not empty (thanks to clubnite)
- Small changes in code to improve Joomla compatibility (thanks to clubnite)
- CSS and JavaScrit could be overriden in template now
- Inputs are using jform array now

#### 1.0.2

- it-IT translation - thanks to Fabio Perri at Transifex
- Joomla 3.0 compatibility

#### 1.0.1

- Small changes to follow JED rules

#### 1.0.0

- Initial release