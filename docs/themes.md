Themes
======

Simple theme
------------

![Simple theme preview](images/theme-simple.png)

Simple theme with no additional graphics. This theme is good start point for creating
[template override](styling.md).

Dark icons theme
----------------

![Dark icons theme preview](images/theme-dark.png)

Simple theme with image based icons. Icons are dark, so this theme is best suitable
for light backgrounded sites.

You can easilly change used icons by [template override](styling.md).

Light icons theme
-----------------

![Light icons theme preview](images/theme-light.png)

Simple theme with image based icons. Icons are light, so this theme is best suitable
for dark backgrounded sites.

You can easilly change used icons by [template override](styling.md).

Bootstrap theme
---------------

![Bootstrap theme preview](images/theme-bootstrap.png)

This theme uses standard Bootstrap v2.3 clasess to achieve its design. No other css 
is used. Bootstrap v2.3 is included in Joomla 3.x, no need to install it. 

Note that __Bootstrap is NOT activated and/or loaded automatically__ by this theme, 
this is up to your template to support and load Bootstrap. If you __do not have 
Bootstrap enabled template do not use this theme__, as it will not work.

Bootstrap 3 theme
------------------

![Bootstrap v3 theme preview](images/theme-bootstrap3.png)

This theme uses standard Bootstrap v3 clasess to achieve its design. Small custom css 
is used to change the input size. Bootstrap v3 is NOT included in any Joomla version by default,
your template needs to support it, or you have to install it and enabled other way. 

Note that __Bootstrap 3 is NOT activated and/or loaded automatically__ by this theme, 
this is up to your template to support and load Bootstrap 3. If you __do not have 
Bootstrap 3 enabled template do not use this theme__, as it will not work.

Bootstrap 4 theme
------------------

![Bootstrap v4 theme preview](images/theme-bootstrap4.png)

This theme uses standard Bootstrap v4 clasess to achieve its design. Small custom css
is used to change the input size. Bootstrap v4 is NOT included in Joomla 3.x by default,
your template needs to support it, or you have to install it and enabled other way.

Note that __Bootstrap 4 neither Font Awesome is NOT activated and/or loaded automatically__
by this theme, this is up to your template to support and load it. If you __do not have
Bootstrap 4 and Font Awesome enabled template do not use this theme__, as it will not work.
