Settings
========

CAPTCHA settings
----------------

#### Enable CAPTCHA

If this option is off, no visual CAPCHA code will be displayed on your forms. Note, 
that this should be used ONLY with combination of some Additional protection enabled,
otherwise your forms will remain completely without protection. 

#### Theme

Select design and look of Captcha field. You can select either _None_, which will 
not include any special CSS or HTML classes, or one of pre-made [themes](themes.md).

If you select _None_, you should take care of styling in your template. If you select 
one of pre-made [themes](themes.md), you can easilly modify its look by 
[template override](styling.md).  

Note that, as n3t Seznam Captcha uses hosted solution, there is no way, how to 
influence design or colors of Captcha image itself. 

#### Display audio challenge

Note that, as n3t Seznam Captcha uses hosted solution, there is no way, how to 
add other language support. 

Additional protection
---------------------

These settings allows you to enable one or more additinal protection services.
Note, that usually one of these services is enough. If you enable all of these
you risk to slow down your forms.

If user doesn't pass any of these checks, his form submition will be canceled
(same way, as if he entered wrong Captcha code) and error message `Your IP address 
(0.0.0.0) is blacklisted. If you feel that it is mistake, please contact this site 
administrator.`, where 0.0.0.0 will be replaced with users current IP address.

#### StopForumSpam.com

StopForumSpam.com is online free database collecting information about spammers
based on their activity. It supports to check its database for username, email and/or
IP address. n3t Seznam Captcha currently supports only IP address check, as this 
is the only relevant information it could reach. If it is listed in StpForumSpam 
database, submitting of form is rejected. 

More information could be found [here][StopForumSpam].

#### BotScout

Another free service, which enables to check username, email and/or IP address.
Again (as noted above) n3t Seznam Captcha checks for IP address only. If it is listed, 
submitting of form is rejected.

More information could be found [here][BotScout].

#### BotScout API key

To enable BotScout protection you need to enter here their API key. This is required 
only if your site will run more than aprox. 20 checks a day, which, in real life,
you will reach very fast. You can get an API key by registerring on their 
[website][BotScout].  

#### SpamHaus project

SpamHaus is DNS-based service, providing list of IP adresses used for spamming.
n3tSeznam Captcha is checking its zen.spamhaus.org list, which is combination of
all SpamHaus lists.

More information could be found [here][SpamHaus].

#### Sorbs.net

Sorbs.net is another DNS-based free service, allowing to check IP address against 
its list.

More information could be found [here][Sorbs].

#### SpamCop.net

SpamCop.net is another DNS-based free service, allowing to check IP address against 
its list. 

More information could be found [here][SpamCop].

#### Project HoneyPot

Project HoneyPot is one of the most famous and well known free service fighting 
against spam. To use this service, you have to obtain an API key (see bellow).

More information could be found [here][ProjectHoneyPot].

#### Project HoneyPot API key

To enable Project Honeypot you have to fill here an API key. This is an 12-characters 
in length, lower case, only alpha characters (no numbers) string, which could be 
requested at [Project HoneyPot][ProjectHoneyPot] site. 

#### IP address whitelist

Whitelisted IP address list. Fill here any IP address (one per line), or IP address 
range (in CIDR format or format 1.2.3.4-1.2.3.5), that would be always taken as 
non spamming, it will not be checked against any additional protection server, 
even additional protection is enabled.

Whitelisted IP addresses are however still checked for correct CAPTCHA answer, 
if Captcha is enabled.

#### IP address blacklist

Blacklisted IP address list. Fill here any IP address (one per line), or IP address 
range (in CIDR format or format 1.2.3.4-1.2.3.5), that would be always blocked. 
These IP addresses will not be checked against any additional protection services, 
and always will be resolved as spammers.  

[StopForumSpam]: http://www.stopforumspam.com/
[BotScout]: http://www.botscout.com/
[SpamHaus]: http://www.spamhaus.org/
[Sorbs]: http://www.sorbs.net/
[SpamCop]: https://www.spamcop.net/
[ProjectHoneyPot]: http://www.projecthoneypot.org/