<?php
/**
 * @package n3t Seznam Captcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2012-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
echo JHtml::_('image','plg_n3tseznamcaptcha/captcha.png', JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CAPTCHA'),'class="seznam-captcha-image" width="170" height="70"',true); 
?>
<div class="seznam-captcha-controls">
  <?php if ($this->params->def('show_audio', 1)) { ?>
  <a href="#" title="<?php echo JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_AUDIO_TITLE'); ?>" class="seznam-captcha-audio"><span><?php echo JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_AUDIO'); ?></span></a>
  <?php } ?>
  <a href="#" title="<?php echo JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_RELOAD_TITLE'); ?>" class="seznam-captcha-reload"><span><?php echo JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_RELOAD'); ?></span></a>
</div>
<input type="hidden" name="n3t_seznam_captcha_hash" value="" />
<input type="text" name="n3t_seznam_captcha" id="jform_captcha" class="seznam-captcha-answer required" size="5" maxlength="5" autocomplete="off" placeholder="<?php echo JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_PLACEHOLDER'); ?>" title="<?php echo JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_TITLE'); ?>" aria-required="true" required="required" />