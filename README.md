[![Documentation Status](https://readthedocs.org/projects/n3t-seznam-captcha/badge/?version=latest)](http://n3tseznamcaptcha.docs.n3t.cz/en/latest/?badge=latest)

n3t Seznam Captcha
==================

n3t Seznam Captcha plugin for Joomla! wraps the seznam.cz Captcha API. It provides 
simple letters based Captcha with optional audio challenge (in Czech language only).

Additional protection by checking online spam databases and blacklists could be 
activated in the configuration. Currently StopForumSpam, BotScout, SpamHaus, Sorbs, 
SpamCop and project HoneyPot are supported. There is also possibility to manually
enter IP blacklist and/or whitelist.

There is also option to disable visual CAPTCHA control, and use just additional 
protection.

Audio challenge should be played automatically in most modern browsers (including 
mobile devices) and even older IE versions. For browsers without audio support, 
there is automatic fallback download link, that will neable your visitors to download 
audio challenge as WAV file and play it on their device any other way.

n3t Seznam Captcha comes with few ready-to use template styles, or could be simply 
styled in template. Currently supported built-in themes are 

 * Simple theme 
 * Light theme
 * Dark Theme
 * Bootstrap theme
 * Bootstrap 3 Theme
 
Documentation
-------------

Find more at [documentation page](http://n3tseznamcaptcha.docs.n3t.cz/en/latest/)        