<?php
/**
 * @package n3t Seznam Captcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2012-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Form\FormHelper;
use Joomla\Utilities\IpHelper;

FormHelper::loadFieldClass('textarea');

class JFormFieldIPList extends JFormFieldTextarea
{

	protected $type = 'IPList';

	protected function getInput()
	{
    jimport( 'fof.utils.ip.ip' );

    if (class_exists('FOFUtilsIp'))
      $ip = FOFUtilsIp::getIp();
    else
      $ip = IpHelper::getIp();

    JHtml::_('jquery.framework');
    $button = '<br />';
    $onclick = "jQuery('#jform_params_" . $this->element['name'] . "').val(jQuery('#jform_params_" . $this->element['name'] . "').val()+(jQuery('#jform_params_" . $this->element['name'] . "').val() ? '\\n' : '')+'" . $ip . "'); return false;";
    $button.= '<button class="btn btn-outline-success btn-sm" onclick="' . $onclick . '" href="#">' . JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CFG_IP_FILTER_ADD_CURRENT') . '</button>';
    $onclick = "jQuery('#jform_params_" . $this->element['name'] . "').val(''); return false;";
    $button.= ' <button class="btn btn-outline-danger btn-sm" onclick="' . $onclick . '">' . JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CFG_IP_FILTER_CLEAR') . '</button>';

    return parent::getInput().$button;
	}
}
