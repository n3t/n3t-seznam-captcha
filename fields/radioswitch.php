<?php
/**
 * @package n3t Seznam Captcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2012-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Form\FormHelper;

FormHelper::loadFieldClass('radio');

class JFormFieldRadioSwitch extends JFormFieldRadio
{

	protected $type = 'RadioSwitch';

  protected function getOptions()
  {
    $tmp = array(
      JHtml::_('select.option', '0', JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CFG_FALSE'), 'value', 'text'),
      JHtml::_('select.option', '1', JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CFG_TRUE'), 'value', 'text')
    );
    return array_merge($tmp,parent::getOptions());
  }

  protected function getLabel()
  {
    $more='';
    if ($this->element['more']) {
      $more.= '<a href="'.$this->element['more'].'" target="_blank">'.JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CFG_MORE_INFORMATION').'</a>';
    }
    return preg_replace('~</label>~','<br />'.$more.'</label>',parent::getLabel());
  }

}
